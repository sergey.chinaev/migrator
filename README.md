# Migrator

Migrator of Golite ecosystem.

## Test coverage
- migrate.go - 95,2%

## Установка
```shell
go get "gitlab.com/golight/migrator"
```
## Доступные функции и методы
### Новый Migrator
```go
//Функция, которая создает новый экземпляр мигратора с заданными параметрами. Она принимает коннект бд, конфигурацию бд и сканер таблиц.
func NewMigrator(db *sqlx.DB, dbConf params.DB, scanner scanner.Scanner)
```

```go
//Метод, который делает миграцию таблицы.
func (m *Migrator) Migrate()
```

## Пример использования

```go
package main

import (
    "gitlab.com/golight/migrator"
    "gitlab.com/golight/dao/params"
    "gitlab.com/golight/scanner"
    "log"
)

type ExchangeUserKeyDTO struct {
    ID         int            `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" mapper:"id" db_ops:"id"`
    ExchangeID int            `json:"exchange_id" db:"exchange_id" db_ops:"create" db_type:"int" db_default:"default 1" mapper:"exchange_id"`
    UserID     int            `json:"user_id" db:"user_id" db_ops:"create" db_type:"int" db_default:"default 1" mapper:"user_id"`
    Label      string         `json:"label" db:"label" db_ops:"create,update" db_type:"varchar(55)" db_default:"default 0" mapper:"label"`
    MakeOrder  bool           `json:"make_order" db:"make_order" db_ops:"create,update" db_type:"boolean" db_default:"default true" mapper:"make_order"`
    APIKey     string         `json:"api_key" db:"api_key" db_ops:"create,update" db_type:"varchar(144)" db_default:"not null" mapper:"api_key"`
    SecretKey  string         `json:"secret_key" db:"secret_key" db_ops:"create,update" db_type:"varchar(144)" db_default:"not null" mapper:"secret_key"`
    CreatedAt  time.Time      `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index" db_ops:"created_at" mapper:"created_at"`
    UpdatedAt  time.Time      `json:"updated_at" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index" mapper:"updated_at"`
    DeletedAt  types.NullTime `json:"deleted_at" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index" db_ops:"deleted_at" mapper:"deleted_at"`
}

func (s *ExchangeUserKeyDTO) TableName() string {
    return "exchange_user_key"
}

func (s *ExchangeUserKeyDTO) OnCreate() []string {
    return []string{}
}

func (s *ExchangeUserKeyDTO) FieldsPointers() []interface{} {
    return []interface{}{}
}
func main() {
    db, err := sqlx.Connect("postgres", "user=foo dbname=bar sslmode=disable")
    if err != nil {
        log.Fatalln(err)
    }

    scan := scanner.NewTableScanner()
    scan.RegisterTable(&ExchangeUser{})

    m := migrator.NewMigrator(db, params.DB{
        Driver: "postgres",
    }, scan)

    err = m.Migrate()
    if err != nil {
        log.Fatalf("What happened: %v", err)
    }
}


```
