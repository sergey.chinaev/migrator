package migrator_test

import (
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/scanner"
	"testing"

	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"gitlab.com/golight/dao/types"
	"gitlab.com/golight/migrator"
	"regexp"
	"time"
)

type TestMigrate struct {
	ID         int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null" mapper:"id" db_ops:"id"`
	ExchangeID int       `json:"exchange_id" db:"exchange_id" db_ops:"create" db_type:"int" db_default:"default 1" mapper:"exchange_id"`
	UserID     int       `json:"user_id" db:"user_id" db_ops:"create" db_type:"int" db_default:"default 1" mapper:"user_id"`
	Label      string    `json:"label" db:"label" db_ops:"create,update" db_type:"varchar(55)" db_default:"default 0" mapper:"label"`
	MakeOrder  bool      `json:"make_order" db:"make_order" db_ops:"create,update" db_type:"boolean" db_default:"default true" mapper:"make_order"`
	APIKey     string    `json:"api_key" db:"api_key" db_ops:"create,update" db_type:"varchar(144)" db_default:"not null" mapper:"api_key"`
	SecretKey  string    `json:"secret_key" db:"secret_key" db_ops:"create,update" db_type:"varchar(144)" db_default:"not null" mapper:"secret_key"`
	CreatedAt  time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index" db_ops:"created_at" mapper:"created_at"`
	UpdatedAt  time.Time `json:"updated_at" db:"updated_at" db_ops:"update" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index" mapper:"updated_at"`
	//nolint:staticcheck
	DeletedAt types.NullTime `json:"deleted_at" db:"deleted_at" db_ops:"update" db_type:"timestamp" db_default:"default null" db_index:"index" db_ops:"deleted_at" mapper:"deleted_at"`
}

func (t *TestMigrate) TableName() string {
	return "test_migrate"
}

func (t *TestMigrate) OnCreate() []string {
	return []string{}
}

func (t *TestMigrate) FieldsPointers() []interface{} {
	return []interface{}{}
}

type TestMigrateAlterTable struct {
	Label string `json:"label" db:"label" db_ops:"create,update" db_type:"varchar(55)" db_default:"default 0" mapper:"label"`
}

func (t *TestMigrateAlterTable) TableName() string {
	return "test_migrate_alter_table"
}

func (t *TestMigrateAlterTable) OnCreate() []string {
	return []string{"label"}
}

func (t *TestMigrateAlterTable) FieldsPointers() []interface{} {
	return []interface{}{&t.Label}
}

func TestMigrator_Migrate(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	dbx := sqlx.NewDb(db, "sqlmock")

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \$1 AND TABLE_SCHEMA = \$2`).
		WithArgs("test_migrate", "public").WillReturnRows(&sqlmock.Rows{})

	mock.ExpectExec(regexp.QuoteMeta("create table test_migrate ( id BIGSERIAL primary key not null, exchange_id int default 1, user_id int default 1, label varchar(55) default 0, make_order boolean default true, api_key varchar(144) not null, secret_key varchar(144) not null, created_at timestamp default (now()) not null, updated_at timestamp default (now()) not null, deleted_at timestamp default null )")).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_created_at_idx on test_migrate (created_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_updated_at_idx on test_migrate (updated_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_deleted_at_idx on test_migrate (deleted_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	scan := scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrate{})

	m := migrator.NewMigrator(dbx, params.DB{
		Driver: "postgres",
	}, scan)

	err = m.Migrate()
	if err != nil {
		t.Errorf("migrator.Migrate() err = %v", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestMigrator_Migrate_AlterTable(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	dbx := sqlx.NewDb(db, "sqlmock")

	columns := []string{"column1", "column2", "column3"}

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \$1 AND TABLE_SCHEMA = \$2`).
		WithArgs("test_migrate_alter_table", "public").
		WillReturnRows(sqlmock.NewRows([]string{"COLUMN_NAME"}).
			AddRow(columns[0]).
			AddRow(columns[1]).
			AddRow(columns[2]))

	mock.ExpectQuery(regexp.QuoteMeta(`alter table test_migrate_alter_table add label varchar(55) default 0`)).
		WillReturnRows(sqlmock.NewRows([]string{}))

	scan := scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrateAlterTable{})

	m := migrator.NewMigrator(dbx, params.DB{
		Driver: "postgres",
	}, scan)

	err = m.Migrate()
	if err != nil {
		t.Errorf("migrator.Migrate() err = %v", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}

}

func TestMigrator_MigrateErrors(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	dbx := sqlx.NewDb(db, "sqlmock")

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \$1 AND TABLE_SCHEMA = \$2`).
		WithArgs("test_migrate", "public").WillReturnError(fmt.Errorf("some err"))

	scan := scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrate{})

	m := migrator.NewMigrator(dbx, params.DB{
		Driver: "postgres",
	}, scan)

	err = m.Migrate()
	if err == nil {
		t.Errorf("migrator.Migrate() no erros")
	}

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \$1 AND TABLE_SCHEMA = \$2`).
		WithArgs("test_migrate", "public").WillReturnRows(&sqlmock.Rows{})

	scan = scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrate{})

	m = migrator.NewMigrator(dbx, params.DB{
		Driver: "postgres",
	}, scan)

	err = m.Migrate()
	if err == nil {
		t.Errorf("migrator.Migrate() no erros")
	}

	columns := []string{"column1", "column2", "column3"}

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \$1 AND TABLE_SCHEMA = \$2`).
		WithArgs("test_migrate_alter_table", "public").
		WillReturnRows(sqlmock.NewRows([]string{"COLUMN_NAME"}).
			AddRow(columns[0]).
			AddRow(columns[1]).
			AddRow(columns[2]))

	mock.ExpectQuery(regexp.QuoteMeta(`alter table test_migrate_alter_table add label varchar(55) default 0`)).
		WillReturnError(fmt.Errorf("some err"))

	scan = scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrateAlterTable{})

	m = migrator.NewMigrator(dbx, params.DB{
		Driver: "postgres",
	}, scan)

	err = m.Migrate()
	if err == nil {
		t.Errorf("migrator.Migrate() no erros")
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestMigratorMigrate_Mysql(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()
	dbx := sqlx.NewDb(db, "sqlmock")

	mock.ExpectQuery(`SELECT COLUMN_NAME FROM INFORMATION_SCHEMA\.COLUMNS WHERE TABLE_NAME = \? AND TABLE_SCHEMA = \?`).
		WithArgs("test_migrate", "").WillReturnRows(&sqlmock.Rows{})

	mock.ExpectExec(regexp.QuoteMeta("create table test_migrate ( id BIGSERIAL primary key not null, exchange_id int default 1, user_id int default 1, label varchar(55) default 0, make_order boolean default true, api_key varchar(144) not null, secret_key varchar(144) not null, created_at timestamp default (now()) not null, updated_at timestamp default (now()) not null, deleted_at timestamp default null )")).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_created_at_idx on test_migrate (created_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_updated_at_idx on test_migrate (updated_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	mock.ExpectExec(regexp.QuoteMeta("create index test_migrate_deleted_at_idx on test_migrate (deleted_at)")).
		WillReturnResult(sqlmock.NewResult(0, 0))

	scan := scanner.NewTableScanner()
	scan.RegisterTable(&TestMigrate{})

	m := migrator.NewMigrator(dbx, params.DB{
		Driver: "mysql",
	}, scan)

	err = m.Migrate()
	if err != nil {
		t.Errorf("migrator.Migrate() err = %v", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
